import java.util.Scanner;
import java.math.BigInteger;

public class Gravitacija {
    public static void main(String[] args) {
        Scanner drejc = new Scanner(System.in);
        int nadVisina = drejc.nextInt();
        
        System.out.printf("%.2f\n", izracun(nadVisina));
    }
    
    
    public static double izracun(int visina){
        //gravKonst = 0,000000000006674
        //masaZ = 5972000000000000000000000
        //konstanta je masaZ*gravKonst
        
        BigInteger konstanta =  new BigInteger("398571000000000");
        long premer = 6371000;
        
        double konst2 = konstanta.doubleValue();
        double pospesek = 9.81;
        
        pospesek = (konst2)/((premer+visina)*(premer+visina));
        
        return pospesek;
    }
}